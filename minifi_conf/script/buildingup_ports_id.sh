#!/bin/bash

if [ "$#" -eq 0 ]; then
        echo "Provide one argument for ID of From MiNiFi0 port in MiNiFi config template xml file"
        exit 1

fi

CONF_FILE="test.xml"
YML_FILE="test.yml"
TEMP="temp"
END=63

MAINSPACE="$HOME"
JARVIS_DIR="$MAINSPACE/deploy-minifi"
CODEDEPLOY_MINIFI_DIR="$MAINSPACE/CodeDeploy_MiNiFi"
MINIFI_PACK_DIR="$JARVIS_DIR/minifi"
MINIFI_DIR="$MINIFI_PACK_DIR/minifi-0.5.0"
MINIFI_TOOLKIT="$MINIFI_PACK_DIR/minifi-toolkit/minifi-toolkit-0.5.0/bin"
MINIFI_CONF_DIR="$MINIFI_DIR/conf"
WORKSPACE="$JARVIS_DIR/workspace"

TEMPLATE_PORT_ID=$1
$MINIFI_TOOLKIT/config.sh transform $CONF_FILE $YML_FILE 

cat $YML_FILE | grep -B 1 "From MiNiFi.*" > $TEMP

rm out

cat $TEMP | grep -B 1 "From MiNiFi$" | grep "id" | awk '{print $3}' >> out

for i in $(seq 1 $END); do
    padded=`printf "%02d\n" $i`
    cat $TEMP | grep -B 1 "From MiNiFi$padded$" | grep "id" | awk '{print $3}' >> out
done


declare -a Array
Array=(`cat out`)
CONF_DIR="$JARVIS_DIR/minifi_conf/script/conf"
rm -rf $CONF_DIR/*

for (( i=0; i<${#Array[@]}; i++ )); do    
    target_file=$CONF_DIR/from-minifi$i
    cp $YML_FILE $target_file
    #sed -i "182s~name: UpdateAttribute/success/.*~name: UpdateAttribute/success/${Array[i]}~" $target_file
    sed -i "s~name: UpdateAttribute/success/$TEMPLATE_PORT_ID~name: UpdateAttribute/success/${Array[i]}~" $target_file
    #sed -i "186s~destination id: .*~destination id: ${Array[i]}~" $target_file
    sed -i "s~destination id: $TEMPLATE_PORT_ID~destination id: ${Array[i]}~" $target_file
    #sed -i "192s~name: UpdateAttribute/success/.*~name: UpdateAttribute/success/${Array[i]}~" $target_file
    sed -i "s~name: UpdateAttribute/success/$TEMPLATE_PORT_ID~name: UpdateAttribute/success/${Array[i]}~" $target_file
    #sed -i "196s~destination id: .*~destination id: ${Array[i]}~" $target_file
    sed -i "s~destination id: $TEMPLATE_PORT_ID~destination id: ${Array[i]}~" $target_file
done

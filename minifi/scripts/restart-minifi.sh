read -p "Enter jarvis-minifi path [/home/ubuntu/jarvis-minifi]: " MINIFIPATH
MINIFIPATH=${MINIFIPATH:-"/home/ubuntu/jarvis-minifi"}

read -p "Should we reset code from git? [Yes]: " RESETFROMGIT
RESETFROMGIT=${RESETFROMGIT:-"Yes"}
if [ "$RESETFROMGIT" = "Yes" ]; then
	cd "$MINIFIPATH/minifi/scripts"
	/bin/bash reset-git.sh
fi 

cd "$MINIFIPATH/minifi/minifi-0.5.0/conf"

read -p "Does MiNiFi have remote process group? [Yes]: " RPGEXISTS
RPGEXISTS=${RPGEXISTS:-"Yes"}

if [ "$RPGEXISTS" = "Yes" ]; then
	echo "Updating NiFi IP address in config file"
	#IP=`ifconfig eth0 2>/dev/null|awk '/inet addr:/ {print $2}'|sed 's/addr://'`
	read -p "Enter NiFi IP address [172.31.18.29]: " IP
	IP=${IP:-"172.31.18.29"}
	sed -i "s/url:.*/url: http:\/\/$IP:8080\/nifi/" config.yml
fi

INPUTPATH=`find $MINIFIPATH -xdev 2>/dev/null -name "operator-profile-input-data"`
sed -i "s|Input Directory:.*|Input Directory: $INPUTPATH|" config.yml
sed -i "s|MY_PROPERTY:.*|MY_PROPERTY: $MINIFIPATH\/minifi\/minifi_custom.cfg|" config.yml

# Check if config file exists
if [ ! -f "$MINIFIPATH/minifi/minifi_custom.cfg" ]; then
	echo "Config file minifi_custom.cfg does not exist, fix it"
	exit 1
fi

echo "Confirmed that config file minifi_custom.cfg file exists"
#read -p "Debug mode? [VERBOSE]: " DEBUGMODE
#DEBUGMODE=${DEBUGMODE:-"VERBOSE"}
#if [ "$DEBUGMODE" = "VERBOSE" ]; then
#	echo "VERBOSE" > "$MINIFIPATH/minifi/minifi_custom.cfg"
#else
#	echo "NONE" > "$MINIFIPATH/minifi/minifi_custom.cfg"
#fi

cd "$INPUTPATH"
echo "Displaying input folder content: " 
ls
read -p "Is input ok? [YES]: " INPUTGOOD
INPUTGOOD=${INPUTGOOD:-"YES"}
if [ "$INPUTGOOD" != "YES" ]; then
	read -p "Provide clean up command: " CLEANUPCOMMAND
	eval "$CLEANUPCOMMAND"
	echo "Displaying input folder content after cleanup: " 
	ls
	read -p "Is input ok now? [YES]: " INPUTGOODLATEST
	INPUTGOODLATEST=${INPUTGOODLATEST:-"YES"}
	if [ "$INPUTGOODLATEST" != "YES" ]; then
		echo "Fix input files first before running minfii"
		exit 1
	fi
fi

cd "$MINIFIPATH/minifi/minifi-0.5.0/bin"
./minifi.sh stop
rm -rf ../content_repository/* ../provenance_repository/* ../flowfile_repository/* ../state/local/* ../logs/*
./minifi.sh start

read -p "Should we impose CPU limit [No]: " CPULIMIT
CPULIMIT=${CPULIMIT:-"No"}
if [ "$CPULIMIT" != "No" ]; then
    echo "Imposing CPU limit of $CPULIMIT" 
    ps -ax | grep [o]rg.apache.nifi.minifi.MiNiFi | cut -d' ' -f2 | xargs -t cpulimit -l $CPULIMIT -p &
fi 

read -p "Should we profile CPU usage [Yes]: " CPUPROFILE
CPUPROFILE=${CPUPROFILE:-"Yes"}
if [ "$CPUPROFILE" = "Yes" ]; then
	MINIFIPID=$(ps -ax | grep [o]rg.apache.nifi.minifi.MiNiFi | awk '{print $1}')
	echo "sleeping for 0.5m"   
	sleep 0.25m

	echo "sampling CPU usage" 
	#cpustat -x -D -a -n 1 1 15 
	cpustat -x -D -a -p $MINIFIPID 1 15 

	echo "sleeping for 0.5m"   
	sleep 0.5m

	echo "sampling CPU usage" 
	#cpustat -x -D -a -n 1 1 15
	cpustat -x -D -a -p $MINIFIPID 1 15
fi

read -p "Stop MiNiFi now? [Yes]: " STOPMINIFI
STOPMINIFI=${STOPMINIFI:-"Yes"}
if [ "$STOPMINIFI" = "Yes" ]; then
	cd "$MINIFIPATH/minifi/minifi-0.5.0/bin"
	sudo ./minifi.sh stop 
	cd ../logs/ 
	echo "Parsing results" 
	python3.5 ../../scripts/profile-operators.py minifi-app.log
fi 

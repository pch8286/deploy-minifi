#!/usr/bin/py
import sys
import statistics
import random
import string

# Example usage: python synthesize-hot-keys-grouping.py xxa-100records 2 0.8 3

file_name=sys.argv[1]
column=int(sys.argv[2])
fractions=[]
for i in range(3,len(sys.argv) - 1):
	fractions.append(sys.argv[i])
totalGroups=int(sys.argv[-1])

numLines=0

# Get line number count in file
with open(file_name,'r') as fp:
	line=fp.readline()
	while line:
		numLines+=1
		line=fp.readline()

grpCounts=[]
numRecordsPending=numLines
for i in range(0,len(fractions)):
	grpCounts.append(int(float(fractions[i]) * numLines))
	numRecordsPending=numRecordsPending-grpCounts[-1]

remainingGroups=totalGroups-len(fractions)
numRecordsPerColdGroup=numRecordsPending/remainingGroups
for i in range(0,remainingGroups):
	grpCounts.append(numRecordsPerColdGroup)


grpIds=set()
grpId=""
# Num of groups
for i in range(len(grpCounts)):
	for j in range(13):
        	grpId+=random.choice(string.ascii_letters)
	grpIds.add(grpId)
	grpId=""

# For each group
lineNums=random.sample(range(numLines), numLines)
listOfGrpIds=list(grpIds)


# Steps, given number of required groups
# - Generate unique group keys of length 13, for the required number of groups
# - Divide #records by #groups to get number of records/group
# - create data

# Actually update lines
lineCount=0
out_f_name=file_name+"-mod"
out_f=open(out_f_name,"a")

grpWithLineNum=[]
currGrpCount=0

lineToGrpMap={}
grpCntIdx=0
accGrpCount=grpCounts[grpCntIdx]
grpId=listOfGrpIds[grpCntIdx]
for i in range(numLines):
	if i/accGrpCount!=0:
		grpCntIdx+=1
		accGrpCount+=grpCounts[grpCntIdx]
		grpId=listOfGrpIds[grpCntIdx]
	lineToGrpMap[i]=grpId

print(lineToGrpMap)
print("Done")
with open(file_name, 'r') as fp:
	line=fp.readline()
	while line:
		line.rstrip()
		splitLine=line.split(",")
		indexOfLine=lineNums.index(lineCount)
		print(lineToGrpMap[indexOfLine])
		splitLine[column]=lineToGrpMap[indexOfLine]
		out_f.write(str(splitLine[0]))
		for col in range(1,14):
			out_f.write(","+str(splitLine[col]))
		line=fp.readline()
		lineCount+=1

out_f.close()

#!/usr/bin/py
import sys
import statistics
import random

file_name=sys.argv[1]
fraction=sys.argv[2]
num_lines=0

# Get line number count in file
with open(file_name,'r') as fp:
	line=fp.readline()
	while line:
		num_lines+=1
		line=fp.readline()

lines_to_update=int(float(fraction)*num_lines)
line_nums=random.sample(range(num_lines),lines_to_update)
print(line_nums)

# Actually update lines
line_count=0
out_f_name=file_name+"-mod"
out_f=open(out_f_name,"a")
with open(file_name, 'r') as fp:
	line=fp.readline()
	while line:
		line.rstrip()
		split_line=line.split(",")
		if line_count in line_nums:
			split_line[10]="-1"
		else:
			split_line[10]="0"
		out_f.write(str(split_line[0]))
		for col in range(1,14):
			out_f.write(","+str(split_line[col]))
		line=fp.readline()
		line_count+=1

out_f.close()

#!/usr/bin/py
import sys
import statistics
import random
import string

if len(sys.argv)!=3:
	print("Number of arguments should be 2: <String based log file name>, <number of groups in terms of fraction of input recrods>")
	sys.exit()

file_name=sys.argv[1]
fraction=sys.argv[2]
num_lines=0

# Get line number count in file
with open(file_name,'r') as fp:
	line=fp.readline()
	while line:
		num_lines+=1
		line=fp.readline()

grp_count=int(float(fraction)*num_lines)

clusters=set()
cluster=""
# Num of groups
for i in range(grp_count):
        curr_len=len(clusters)
        while len(clusters)!=curr_len+1:
                # Num of characters per group key
                for j in range(13):
                        cluster+=random.choice(string.ascii_letters)
                clusters.add(cluster)
                cluster=""

records_per_group=num_lines/grp_count
line_nums=random.sample(range(num_lines), num_lines)
list_of_groups=list(clusters)
print(grp_count)
print(list_of_groups)

# Steps, given number of required groups
# - Generate unique group keys of length 13, for the required number of groups
# - Divide #records by #groups to get number of records/group
# - create data

# Actually update lines
line_count=0
out_f_name=file_name+"-mod"
out_f=open(out_f_name,"a")
with open(file_name, 'r') as fp:
	line=fp.readline()
	while line:
		line.rstrip()
		split_line=line.split(",")
		index_of_line=line_nums.index(line_count)
		group_key=list_of_groups[index_of_line % grp_count]
		split_line[2]=group_key
		out_f.write(str(split_line[0]))
		for col in range(1,14):
			out_f.write(","+str(split_line[col]))
		line=fp.readline()
		line_count+=1

out_f.close()

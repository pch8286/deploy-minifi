#!/bin/bash

if [ $HOSTNAME = "athuls89-XPS-730X" ]; then
	echo "On the right host"
else
	echo "Run on osl server, exiting"
	exit 1
fi

if [ $# != 2 ]; then
    echo "Required arguments: <Full path of input csv file argument to intify and convert to kryo>, <Number of groups> "
    exit 1
fi

fullInputFileName=$1
numGroupsFrac=$2
groupedFileName=$fullInputFileName"-mod-srccluster-grpby-"$numGroupsFrac"grpcount"
inputDataFileFolder="/home/athuls89/Desktop/OSL/msr/minifi/jarvis-minifi/minifi/input_data_bak"

cd $inputDataFileFolder
python3.5 ../scripts/data-synthesis/synthesize-input-group.py $fullInputFileName $numGroupsFrac
sudo mv $fullInputFileName"-mod" $groupedFileName

cd $inputDataFileFolder/intified
python3.5 intifyAllFields.py $groupedFileName

mv $groupedFileName."allintify" $inputDataFileFolder/intified/
cd $inputDataFileFolder/kryoConverter


fullGroupedIntifiedFileName=$groupedFileName."allintify"
baseGroupedIntifiedFileName=$(basename "${fullGroupedIntifiedFileName}")
groupedIntifiedFileName=$inputDataFileFolder/intified/$baseGroupedIntifiedFileName

echo "Number of unique groups is"
python3.5 ../../scripts/data-synthesis/validateGroupCount.py $groupedIntifiedFileName

java -cp rxjavatest.jar:kryo-5.0.0-RC4.jar:objenesis-3.0.1.jar:minlog-1.3.1.jar:reflectasm-1.11.8.jar Main $groupedIntifiedFileName $groupedIntifiedFileName."kryo" SER TRUE

mv $groupedIntifiedFileName."kryo" $inputDataFileFolder/kryoData/

groupedKryoFileName=$groupedIntifiedFileName."kryo"
baseKryoFileName=$(basename "${groupedKryoFileName}")

java -cp rxjavatest.jar:kryo-5.0.0-RC4.jar:objenesis-3.0.1.jar:minlog-1.3.1.jar:reflectasm-1.11.8.jar Main $inputDataFileFolder/kryoData/$baseKryoFileName temp.deser DESER_PINGMESH TRUE

echo "Comparing kryo deserialized and original all intified file"
vimdiff temp.deser $groupedIntifiedFileName

read -p "Looks good? [Yes]: " DONE
DONE=${DONE:-"Yes"}

if [ "$DONE" = "Yes" ]; then
	rm temp.deser
fi

# TODO: need to merge with createNewKryoFile.sh

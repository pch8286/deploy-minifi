import os
import sys

filename=sys.argv[1]
with open(filename, "r") as fp:
	line=fp.readline()
	while line:
		if "Queue size" in line:
	        	vals=line.split(" ")
        		print(str(vals[len(vals)-1]).rstrip())
		line=fp.readline()

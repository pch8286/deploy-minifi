read -p "Enter jarvis-minifi path [/home/ubuntu/jarvis-minifi]: " MINIFIPATH
MINIFIPATH=${MINIFIPATH:-"/home/ubuntu/jarvis-minifi"}
cd "$MINIFIPATH/minifi"
sudo git checkout -- .
sudo git clean -fd
sudo git pull
sudo git pull
read -p "Copy older libs from home directory? [Yes]: " RELOADLIB
RELOADLIB=${RELOADLIB:-"Yes"}
if [ "$RELOADLIB" = "Yes" ]; then
	sudo cp /home/ubuntu/nifi-cloud-nar-1.0-SNAPSHOT.nar "$MINIFIPATH/minifi/minifi-0.5.0/lib/"
	sudo cp /home/ubuntu/nifi-edge-nar-1.0-SNAPSHOT.nar "$MINIFIPATH/minifi/minifi-0.5.0/lib/"
fi 

sudo chown -R ubuntu "$MINIFIPATH/minifi/minifi-0.5.0/lib/nifi-cloud-nar-1.0-SNAPSHOT.nar" "$MINIFIPATH/minifi/minifi-0.5.0/lib/nifi-edge-nar-1.0-SNAPSHOT.nar"

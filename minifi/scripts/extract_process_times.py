import re
import sys

with open('minifi-app.log', 'r') as fp:
        line=fp.readline()
        while line:
                process_time=re.search('Total processing time: (\d+)',line)
                if process_time:
                        print(process_time.group(1))
                line=fp.readline()

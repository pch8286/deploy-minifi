#!/usr/bin/py
import sys
import statistics

file_name=sys.argv[1]
edge_query_latencies=[]
cloud_query_latencies=[]
network_latencies=[]
overall_latencies=[]
queueing_latencies=[]
extract_latencies=[]
total_latencies=[]
deserialize_latencies=[]
serialize_latencies=[]
flowfile_sizes=[]
edge_query_file_sizes=[]
cloud_query_file_sizes=[]
final_file_sizes=[]

grpByProc_latencies=[]
grpByQ_latencies=[]
aggProc_latencies=[]
aggQ_latencies=[]

with open(file_name,'r') as fp:
	line=fp.readline()
	while line:
		if "m_query_now" in line and "Key" in line:
			line=fp.readline()
			line=line.replace("'","")
			split_line=line.split()
			query_now=float(split_line[1])
		if "m_source_now" in line and "Key" in line:
			line=fp.readline()
			line=line.replace("'","")
			split_line=line.split()
			source_now=float(split_line[1])
			edge_query_latencies.append(query_now-source_now)		
			queueing_latencies.append(queue_end-source_now)
		if "m_queueEndTime" in line and "Key" in line:
			line=fp.readline()
			line=line.replace("'","")
			split_line=line.split()
			queue_end=float(split_line[1])
			deserialize_latencies.append(query_now-queue_end)
		if "Extraction time" in line:
			split_line=line.split(' is ')
			extract_latencies.append(float(split_line[1].rstrip()))
		if "Serialize time" in line:
			split_line=line.split(' is ')
			serialize_latencies.append(float(split_line[1].rstrip()))	
		if "m_grpbyop_avgProcTime" in line and "Key" in line:
			line=fp.readline()
			line=line.replace("'","")
			split_line=line.split()
			grpByProc_latencies.append(float(split_line[1]))
		if "m_grpbyop_maxQTime" in line and "Key" in line:
			line=fp.readline()
			line=line.replace("'","")
			split_line=line.split()
			grpByQ_latencies.append(float(split_line[1]))
		if "m_aggop_avgProcTime" in line and "Key" in line:
			line=fp.readline()
			line=line.replace("'","")
			split_line=line.split()
			aggProc_latencies.append(float(split_line[1]))
		if "epoch duration" in line:
			split_line=line.split(' is ')
			aggProc_latencies.append(float(split_line[1].rstrip()))
		if "m_aggop_maxQTime" in line and "Key" in line:
			line=fp.readline()
			line=line.replace("'","")
			split_line=line.split()
			aggQ_latencies.append(float(split_line[1]))
		if "m_totalDuration" in line and "Key" in line:
			line=fp.readline()
			line=line.replace("'","")
			split_line=line.split()
			total_latencies.append(float(split_line[1]))
		if "origFileSize" in line:
			line=fp.readline()
			line=line.replace("'","")
			split_line=line.split()
			flowfile_sizes.append(float(split_line[1]))
		if "m_query_filesize" in line:
			line=fp.readline()
			line=line.replace("'","")
			split_line=line.split()
			edge_query_file_sizes.append(float(split_line[1]))
		line=fp.readline()

# Display latencies
print("Raw edge_query latency values:")
for query_lat in edge_query_latencies:
	print(query_lat)

if len(edge_query_latencies) > 0: print("Edge query latency:",str(statistics.mean(edge_query_latencies)),"+-",str(statistics.stdev(edge_query_latencies)))
if len(queueing_latencies) > 0: print("Queueing latency:",str(statistics.mean(queueing_latencies)),"+-",str(statistics.stdev(queueing_latencies)))
if len(extract_latencies) > 0: print("Extraction latency:",str(statistics.mean(extract_latencies)),"+-",str(statistics.stdev(extract_latencies)))
if len(serialize_latencies) > 0: print("Serialization latency:",str(statistics.mean(serialize_latencies)),"+-",str(statistics.stdev(serialize_latencies)))
if len(aggProc_latencies) > 0: print("Processing latency:",str(statistics.mean(aggProc_latencies)),"+-",str(statistics.stdev(aggProc_latencies)))
#print("GrpBy latency:",str(statistics.mean(grpBy_latencies)),"+-",str(statistics.stdev(grpBy_latencies)))
if len(grpByProc_latencies) > 0: print("GrpBy Proc latency:",str(statistics.mean(grpByProc_latencies)),"+-",str(statistics.stdev(grpByProc_latencies)))
if len(grpByQ_latencies): print("GrpBy Q latency:",str(statistics.mean(grpByQ_latencies)),"+-",str(statistics.stdev(grpByQ_latencies)))
#print("Agg Proc latency:",str(statistics.mean(aggProc_latencies)),"+-",str(statistics.stdev(aggProc_latencies)))
#print("Agg Q latency:",str(statistics.mean(aggQ_latencies)),"+-",str(statistics.stdev(aggQ_latencies)))

if len(total_latencies) > 0: print("Total latency:",str(statistics.mean(total_latencies)),"+-",str(statistics.stdev(total_latencies)))
if len(deserialize_latencies) > 0: print("Total latency with deserialize:",str(statistics.mean(deserialize_latencies)),"+-",str(statistics.stdev(deserialize_latencies)))

# Flow file sizes
if len(flowfile_sizes) > 0: print("Orig Flowfile sizes:",str(statistics.mean(flowfile_sizes)),"+-",str(statistics.stdev(flowfile_sizes)))
if len(edge_query_file_sizes) > 0: print("Query Flowfile sizes:",str(statistics.mean(edge_query_file_sizes)),"+-",str(statistics.stdev(edge_query_file_sizes)))

# Calculate bandwidth
flowfile_begin=0
flowfile_start=0
flowfile_end=0
flowfile_count=0
source_data_gen=0
source_first_now=-1
source_last_now=0
edge_query_first_now=-1
edge_query_last_now=0
cloud_query_first_now=-1
cloud_query_last_now=0
with open(file_name,'r') as fp:
	line=fp.readline()
	while line:
		if "m_source_flowfile_start" in line:
			line=fp.readline()
			line=line.replace("'","")
			split_line=line.split()
			flowfile_start=int(split_line[1])
			if flowfile_count==0:
				flowfile_begin=flowfile_start
			flowfile_count+=1
		if "origFileSize" in line:
			line=fp.readline()
			line=line.replace("'","")
			split_line=line.split()
			source_data_gen+=float(split_line[1])
		if "m_source_now" in line:
			line=fp.readline()
			line=line.replace("'","")
			split_line=line.split()
			if source_first_now==-1:
				source_first_now=int(split_line[1])
			source_last_now=int(split_line[1])
		if "m_query_now" in line:
                        line=fp.readline()
                        line=line.replace("'","")
                        split_line=line.split()
                        if edge_query_first_now==-1:
                                edge_query_first_now=int(split_line[1])
                        edge_query_last_now=int(split_line[1])
		line=fp.readline()

total_duration=(edge_query_last_now-source_first_now)/1000
source_data_total=source_data_gen/(1024*1024)
source_throughput=source_data_total/((source_last_now-source_first_now)/1000)

# QueryRecord throughput
edge_query_throughput=source_data_total/((edge_query_last_now-edge_query_first_now)/1000)

print("Expected source throughput: ",str(source_throughput)," MBps")
print("Edge Query Thruput: ",str(edge_query_throughput)," MBps")
print("Number of flow files: ", str(flowfile_count))
print("Total duration (sec): ", str(total_duration))

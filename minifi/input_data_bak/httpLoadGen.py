#!/usr/bin/py
import sys
import random
import struct
import socket
from randomtimestamp import randomtimestamp
"""
config_file=sys.argv[1]
with fp as open(config_file,'r'):
	line=fp.read_line()
	while(line):
		line=fp.read_line()
"""
lines=int(sys.argv[1])
curr=0
while(curr<lines):
	random_ip=socket.inet_ntoa(struct.pack('>I', random.randint(1, 0xffffffff)))
	rfc="-"
	user="apache"
	date=randomtimestamp()
	# 0 means GET and 1 means POST
	request_type_dict={0:'GET', 1:'POST'}
	request_type=0
	
	# 0 means /api/user, 1 means /api/user1, 2 means /report
	section_dict={1:'/api/user', 2:'/api/user1', 2:'/report'}
	section=1
	request_suffix="HTTP/1.0"
	
	# 0 means 200, 1 means 404
	status_dict={0:'200', 1:'404'}
	status=0
	
	delay=random.randrange(100,2000)
	line=str(random_ip)+","+rfc+","+user+","+str(date)+","+str(request_type_dict[request_type])+" "+\
	str(section_dict[section])+" "+str(request_suffix)+","+str(status_dict[status])+","+str(delay)
	print(line)
	curr+=1

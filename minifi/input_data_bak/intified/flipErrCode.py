#!/usr/bin/py
import sys
import statistics

#intified file name
file_name=sys.argv[1]

count=0
line_count=0
errcode=0
fieldval_mapping={}
out_file=file_name+".fliperrcode"
fp_out=open(out_file,"w")
with open(file_name,'r') as fp:
	line=fp.readline()
	while line:
		split_line=line.split(",")

		if split_line[10]=='1':
			errcode=0
		else:
			errcode=1

		split_line[10]=errcode
		fp_out.write(split_line[0])
		for col_idx in range(1,len(split_line)):
			fp_out.write(","+str(split_line[col_idx]))
		line=fp.readline()

fp_out.close()


#!/usr/bin/py
import sys
import random
import struct
import socket
import fileinput
from randomtimestamp import randomtimestamp

filter_out=0.2
devices=['CPU utilization', 'GPU utilization', 'memory utilization']
numSrcClusters=1000
base_file=sys.argv[1]
with fileinput.FileInput(base_file, inplace=True, backup='.bak') as file:
	for line in file:
		insert_device_util=round(random.random(),2)
		if(insert_device_util >= filter_out):
			# Insert device util info
			device_id=random.randint(0,2)
			util=random.randint(0,100)
			src_cluster=random.randint(0,numSrcClusters)
			line=line.rstrip('\n')
			line+=", :=source cluster:=" + str(src_cluster) +", :="+ devices[device_id]+":="+str(util)+"%"+'\n'
			#line=line.replace(line, line+ ", " + devices[device_id]+"="+str(util)+"%")
		print(line, end='')

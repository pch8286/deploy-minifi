#!/bin/bash
SHELL=`basename "$0"`

MAINSPACE="$HOME"
JARVIS_DIR="$MAINSPACE/deploy-minifi"
CODEDEPLOY_MINIFI_DIR="$MAINSPACE/CodeDeploy_MiNiFi"
MINIFI_PACK_DIR="$JARVIS_DIR/minifi"
MINIFI_DIR="$MINIFI_PACK_DIR/minifi-0.5.0"
MINIFI_TOOLKIT="$MINIFI_PACK_DIR/minifi-toolkit/minifi-toolkit-0.5.0/bin"
WORKSPACE="$JARVIS_DIR/workspace"
MINIFI_CONF_DIR="$JARVIS_DIR/minifi_conf"

# MiNiFi update if change lib or something inside minifi, not conf
echo "$SHELL: copy minifi files."
rm -rf $WORKSPACE/minifi/minifi-0.5.0 $WORKSPACE/minifi/operator-profile-input-data
cp -r $MINIFI_PACK_DIR/minifi-0.5.0 $WORKSPACE/minifi

### Make sure correct input file to MiNiFi is available for deployment ###
cd $MINIFI_PACK_DIR/operator-profile-input-data
echo "Displaying input folder content: "                                              
ls
read -p "Is input ok? [YES]: " INPUTGOOD                                       
INPUTGOOD=${INPUTGOOD:-"YES"}
if [ "$INPUTGOOD" != "YES" ]; then                                                    
        read -p "Provide clean up command: " CLEANUPCOMMAND
        eval "$CLEANUPCOMMAND"
        echo "Displaying input folder content after cleanup: " 
        ls
        read -p "Is input ok now? [YES]: " INPUTGOODLATEST
        INPUTGOODLATEST=${INPUTGOODLATEST:-"YES"}
        if [ "$INPUTGOODLATEST" != "YES" ]; then
                echo "Fix input files first before running minfii"
                exit 1
        fi
fi
cp -r $MINIFI_PACK_DIR/operator-profile-input-data $WORKSPACE/minifi
###########################################################################

# Check if config file exists
if [ ! -f "$MINIFI_PACK_DIR/minifi_custom.cfg" ]; then
        echo "Config file minifi_custom.cfg does not exist, fix it"
        exit 1
fi

#OPERATOR_INPUT_DIR_TEMPLATE="/home/ubuntu/minifi-repo/jarvis-minifi/minifi/operator-profile-input-data"
#FILENAME=`ls $WORKSPACE/minifi/operator-profile-input-data | head -n 1`
#FULL_INPUT_PATH="${OPERATOR_INPUT_DIR_TEMPLATE}/${FILENAME}"
#sed -i "s|\"inputFile\": \".*\"|\"inputFile\": \"$FULL_INPUT_PATH\"|" $MINIFI_PACK_DIR/minifi_custom.cfg

cat $MINIFI_PACK_DIR/minifi_custom.cfg
read -p "Is config file for MiNiFi looking ok? [YES]: " INPUTGOOD
INPUTGOOD=${INPUTGOOD:-"YES"}
if [ "$INPUTGOOD" != "YES" ]; then
	echo "Fix minifi custom config file contents and rerun"
	exit 1
fi
echo "Confirmed that config file minifi_custom.cfg is good"

cp $MINIFI_PACK_DIR/minifi_custom.cfg $WORKSPACE/minifi
cp $MINIFI_CONF_DIR/script/conf/* $CODEDEPLOY_MINIFI_DIR/conf/
echo "$SHELL: encoding minifi files."
cd $WORKSPACE
tar cvzf - minifi | split -b 40M - minifi.tar.gz.
mv minifi.tar.gz.* $CODEDEPLOY_MINIFI_DIR

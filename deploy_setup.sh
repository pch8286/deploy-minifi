#!/bin/bash
SHELL=`basename "$0"`

CONF_DIR="$HOME/deploy-minifi/minifi_conf/script/conf"
MAINSPACE="$HOME"
JARVIS_DIR="$MAINSPACE/deploy-minifi"
CODEDEPLOY_MINIFI_DIR="$MAINSPACE/CodeDeploy_MiNiFi"
MINIFI_PACK_DIR="$JARVIS_DIR/minifi"
MINIFI_DIR="$MINIFI_PACK_DIR/minifi-0.5.0"
MINIFI_TOOLKIT="$MINIFI_PACK_DIR/minifi-toolkit/minifi-toolkit-0.5.0/bin"
MINIFI_CONF_DIR="$MINIFI_DIR/conf"
WORKSPACE="$JARVIS_DIR/workspace"
IP="172.31.17.44"

for i in $(seq 0 15); do
    echo "$SHELL: transform and send config file."

    cp $CONF_DIR/from-minifi$i ./config.yml

    sed -i "s~minifi_custom.cfg~/home/ubuntu/minifi/minifi_custom.cfg~g" config.yml
    sed -i "s~/home/ubuntu/minifi-repo/jarvis-minifi/minifi/operator-profile-input-data~/home/ubuntu/minifi/operator-profile-input-data~g" config.yml
    # sed -i "s~130.126.136.41~$IP~g" config.yml
    sed -i "s~10.20.18.74~$IP~g" config.yml
    sed -i "s~    max concurrent tasks: 1~    max concurrent tasks: 2~g" config.yml

    mv config.yml $CODEDEPLOY_MINIFI_DIR
    ./push_it.sh "All-SP non-8x-test port$i max con2 All-SP"
done
cd $CODEDEPLOY_MINIFI_DIR
git push
git log | grep "commit" > ../commit.txt

echo "$SHELL: Done."

# # MiNiFi update if change lib or something inside minifi, not conf
#echo "$SHELL: copy minifi files."
#cp -r $MINIFI_PACK_DIR/minifi-0.5.0 $WORKSPACE/minifi
#cp -r $MINIFI_PACK_DIR/operator-profile-input-data $WORKSPACE/minifi

#echo "$SHELL: encoding minifi files."
#cd $WORKSPACE
#tar cvzf - minifi | split -b 40M - minifi.tar.gz.
#mv minifi.tar.gz.* $CODEDEPLOY_MINIFI_DIR

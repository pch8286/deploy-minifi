#!/bin/bash
SHELL=`basename "$0"`

if [ "$#" -eq 0 ]; then
        echo "Argument should provide the commit message"
        exit 1

fi

MAINSPACE="$HOME"
JARVIS_DIR="$MAINSPACE/deploy-minifi"
CODEDEPLOY_MINIFI_DIR="$MAINSPACE/CodeDeploy_MiNiFi"
MINIFI_PACK_DIR="$JARVIS_DIR/minifi"
MINIFI_DIR="$MINIFI_PACK_DIR/minifi-0.5.0"
MINIFI_TOOLKIT="$MINIFI_PACK_DIR/minifi-toolkit/minifi-toolkit-0.5.0/bin"
MINIFI_CONF_DIR="$MINIFI_DIR/conf"
WORKSPACE="$JARVIS_DIR/workspace"

cd $CODEDEPLOY_MINIFI_DIR
git add .
git commit -m "$1"
git push

# Deploy-minifi
- [Deploy-minifi](#deploy-minifi)
  - [Folder](#folder)
  - [Scripts](#scripts)
    - [./minifi_conf/script/buildingup_ports_id.sh](#minifi_confscriptbuildingup_ports_idsh)
    - [./update_minifi.sh](#update_minifish)
      - [Example](#example)
    - [./deploy_setup.sh](#deploy_setupsh)
      - [Example](#example-1)
    - [./push_it.sh](#push_itsh)
      - [Example](#example-2)

## Folder
* CONF_DIR="$HOME/minifi_conf/script/conf"
* MAINSPACE="$HOME/jarvis"
* JARVIS_DIR="$MAINSPACE/deploy-minifi"
* CODEDEPLOY_MINIFI_DIR="$MAINSPACE/CodeDeploy_MiNiFi"
* MINIFI_PACK_DIR="$JARVIS_DIR/minifi"
* MINIFI_DIR="$MINIFI_PACK_DIR/minifi-0.5.0"
* MINIFI_TOOLKIT="$MINIFI_PACK_DIR/minifi-toolkit/minifi-toolkit-0.5.0/bin"
* MINIFI_CONF_DIR="$MINIFI_DIR/conf"
* WORKSPACE="$JARVIS_DIR/workspace"

## Scripts
### ./minifi_conf/script/buildingup_ports_id.sh
* Automatically generate yml files of 64 port based on sample `test.xml`
* **Need to change `$CONF_DIR`**
  * CONF_DIR="/home/chanho/minifi_conf/script/conf"
  * I will 64 config.yml files on `$CONF_DIR`

### ./update_minifi.sh
* Update minifi to `$MAINSPACE/CodeDeploy_MiNiFi` GitHub Folder

#### Example
```console
$ bash update_minifi.sh
```

### ./deploy_setup.sh 
* commit each port config.yml and push them with `./push_it.sh`.
* **Need to change `$CONF_DIR`**
* **Need to change `deploy_test_scaleup.sh` in autoscale scripts** after execute this.
* **Need to change `$IP` if you want to try on different NiFi instance**
  * Default: 8x m5a Jarivs Cloud (32 cores and 128 GB RAM)
* *Change commit message for readability*

#### Example
```console
$ bash deploy_setup.sh
```

### ./push_it.sh
* commit and push in `$CODEDEPLOY_MINIFI_DIR`.
* **Need to change deploy_test_scaleup.sh** if you changed MiNiFi in GitHub

#### Example
```console
$ bash push_it.sh
```
